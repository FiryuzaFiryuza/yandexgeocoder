package com.example.my.yandexgeocoder.Models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by 123 on 15.09.2015.
 */
public class GeoPosition {

    private String Name;

    private LatLng location;


    public GeoPosition(String name, LatLng location) {
        Name = name;
        this.location = location;
    }

    public String getName() {
        return Name;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }
}
