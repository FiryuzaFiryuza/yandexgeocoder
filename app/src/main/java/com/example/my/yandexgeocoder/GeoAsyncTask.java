package com.example.my.yandexgeocoder;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.example.my.yandexgeocoder.Models.GeoPosition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by 123 on 14.09.2015.
 */
public class GeoAsyncTask extends AsyncTask<Void, Void, String> {

    public interface CallBack {
        void onLocationRecieved(ArrayList<GeoPosition> list);
    }

    private LatLng location;

    private final String BASE_URL = "https://geocode-maps.yandex.ru/1.x/?format=json&geocode=";
    private final String EXTRA_PARAMS = "&rspn=0&results=10&kind=locality";

    private CallBack callback;

    public GeoAsyncTask(LatLng location, MapsActivity activity) {
        this.location = location;

        callback = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Void... urls) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(getUrl(location))
                .build();


        Response response = null;

        try {
            response = client.newCall(request).execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            return response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (result != null) {
            callback.onLocationRecieved(parseJSON(result));
        }
    }

    private String getUrl(LatLng location)
    {
        String url = BASE_URL + location.longitude + ", " + location.latitude + EXTRA_PARAMS;

        return url;
    }

    private ArrayList<GeoPosition> parseJSON(String response) {
        JSONObject jObj = new JSONObject();
        JSONArray jArray = new JSONArray();
        ArrayList<GeoPosition> posList = new ArrayList<>();

        try {
            jObj = new JSONObject(response);

            jArray = jObj.getJSONObject("response").getJSONObject("GeoObjectCollection").getJSONArray("featureMember");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < jArray.length(); i++) {
            try {
                JSONObject oneObject = jArray.getJSONObject(i);

                String[] positions = oneObject.getJSONObject("GeoObject").getJSONObject("Point").getString("pos").split(" ");

                LatLng location = new LatLng(new Double(positions[1]), new Double(positions[0]));

                String name = oneObject.getJSONObject("GeoObject").getString("name");

                posList.add(new GeoPosition(name, location));

            } catch (JSONException e) {
                Log.d("json", e.getMessage());
            }
        }

        return posList;
    }
}
